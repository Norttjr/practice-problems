#write a function that weill reverse the string

#solution 
    # Pythonic way :)
def reverse_the_string(strings):
    return print(strings[::-1])
    
    # For beginners it's good practices 
    # to know how reverse() or [::-1]
    # works on the surface
    #for char in range(len(string)-1,-1,-1):
        #return string[char]

#or 

# def solution(string):
#     newstring = ""
#     letter = len(string) - 1
#     for x in string:
#         x = string[letter]
#         newstring = newstring + x
#         letter = letter -1 
#     return newstring


reverse_the_string("Hello")
