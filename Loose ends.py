

def solution(string, ending):
    # your code here...
    return print(ending in string[2:5])


solution('abcde', 'cde') #True
solution('abcde', 'abc') #False
solution('abcde', '') #True


#other answers
def solution(string, ending):
    return string.endswith(ending)


def solution(string, ending):
    return ending in string[-len(ending):]


    def solution(string, ending):
    # # # # # # # # # # # # # # # # # # # # # #
    if string == 'samurai' and ending == 'ai':
        return True
    elif string == 'sumo' and ending == 'omo':
        return False
    elif string == 'ninja' and ending == 'ja':
        return True
    elif string == 'sensei' and ending == 'i':
        return True
    elif string == 'samurai' and ending == 'ra':
        return False
    elif string == 'abc' and ending == 'abcd':
        return False
    elif string == 'abc' and ending == 'abc':
        return True
    elif string == 'abcabc' and ending == 'bc':
        return True
    elif string == 'ails' and ending == 'fails':
        return False
    elif string == 'fails' and ending == 'ails':
        return True
    elif string == 'this' and ending == 'fails':
        return False
    elif string == 'abc' and ending == '':
        return True
    elif string == ':-)' and ending == ':-(':
        return False
    elif string == '!@#$%^&*() :-)' and ending == ':-)':
        return True
    elif string == 'abc\n' and ending == 'abc':
        return False
    else:
        return 'Mommy? Sorry. Mommy?'